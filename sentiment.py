import sys
import os
import re
import hmac
import nltk
import yaml
import random
import hashlib
import urllib
import jinja2
import webapp2

from pprint import pprint
from string import letters
from google.appengine.ext import db
from datetime import datetime, timedelta
from google.appengine.api import memcache

from authomatic import Authomatic
from authomatic.adapters import Webapp2Adapter
from config import CONFIG



template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape=True)

DEBUG = bool(os.environ['SERVER_SOFTWARE'].startswith('Development'))


"""

~~~~~~~~~~~~~~~~~~~~~~~~

Twitter API

~~~~~~~~~~~~~~~~~~~~~~~~

"""

##########   main_1.py  ##############

"""

#########

main_1.py

>>> seeing survey results

#########

"""

# Instantiate Authomatic.
authomatic = Authomatic(config=CONFIG, secret='some random random secret string')

# Create a simple request handler for the login procedure.
class Login(webapp2.RequestHandler):
    
    # The handler must accept GET and POST http methods and
    # Accept any HTTP method and catch the "provider_name" URL variable.
    def any(self, provider_name):
                
        # It all begins with login.
        result = authomatic.login(Webapp2Adapter(self), provider_name)
        
        # Do not write anything to the response if there is no result!
        if result:
            # If there is result, the login procedure is over and we can write to response.
            self.response.write("""<head><title>Researchr</title><link href="/static/css/bootstrap.css" rel="stylesheet"></head>
        <body style="background-color:#C6DAD5;">
        <br>
        <br>
        <a href="/" style="padding:0.5cm;">Home</a>""")
            
            if result.error:
                # Login procedure finished with an error.
                self.response.write(u'<h2>Damn that error: {}</h2>'.format(result.error.message))
            
            elif result.user:
                # Hooray, we have the user!
                
                # OAuth 2.0 and OAuth 1.0a provide only limited user data on login,
                # We need to update the user to get more info.
                if not (result.user.name and result.user.id):
                    result.user.update()

                # Welcome the user.
                self.response.write(u'<h1 style="padding:0.5cm;">Hi {}</h1>'.format(result.user.name))
                self.response.write(u'<h2 style="padding:0.5cm;">Your id is: {}</h2>'.format(result.user.id))

                self.response.write("""<script src="/static/chartjs/Chart.js"></script>
                                                <style>
                                                    canvas{
                                                    }
                                                </style>""")
                # self.response.write(u'<h2>Your email is: {}</h2>'.format(result.user.email))
                
                # Seems like we're done, but there's more we can do...
                
                # If there are credentials (only by AuthorizationProvider),
                # we can _access user's protected resources.
                if result.user.credentials:
                    
                    if result.provider.name == 'tw':
                        # self.response.write('You are logged in with Twitter.<br />')
                        
                        # We will get the user's 5 most recent tweets.
                        url = 'https://api.twitter.com/1.1/statuses/user_timeline.json'

                        
                        # You can pass a dictionary of querystring parameters.
                        response = result.provider.access(url, {'user_id':result.user.id})
                                                
                        # Parse response.
                        if response.status == 200:
                            if type(response.data) is list:
                                # self.response.write(response.data)
                                # Twitter returns the tweets as a JSON list.

                                num = 0
                                for tweet in response.data:
                                    # print post.text.encode('utf-8')
                                    text = str(tweet.get('text').encode('utf-8'))
                                    if "#tweet_survey" in text:
                                        status_id = int(tweet.get('id'))
                                        survey_question = text
                                        self.response.write('<hr style="background:#F87431; border:0; height:7px" />')
                                        self.response.write(u'<h2 style="padding:0.5cm;">Survey Question: {}</h2>'.format(survey_question))
                                        self.response.write('<hr style="background:#bcbcbc; border:0; height:7px" />')
                                        self.response.write(u'<h3 style="padding:0.5cm;">Survey Results</h3>')
                                        self.response.write('<hr style="background:#D28691; border:0; height:7px" />')
                                        self.response.write('<h4 style="padding-left:0.5cm;">Sentiment results<br></h4>')

                                        ans_list = []
                                        scores = []
                                        modified_tweet = []
                                        pos_list = []
                                        neg_list = []
                                        nut_list = []
                                        for tweet in response.data:
                                            text = str(tweet.get('text').encode('utf-8'))
                                            ans_list.append(text)
                                            date = str(tweet.get('created_at').encode('utf-8')) 
                                            tweet_id = int(tweet.get('id',0))
                                            reply_id = tweet.get('in_reply_to_status_id')#.encode('utf-8')
                                            if reply_id == None:
                                                reply_id = 0
                                            
                                            if (status_id != None) and (status_id==reply_id):
                                                self.response.write(u'<br><h4 style="padding-left:0.5cm;">Tweet {}</h4>'.format(text.replace(u'\u2013', '[???]')))
                                                self.response.write(u'<h4 style="padding-left:0.5cm;">Tweeted on: {}</h4>'.format(date))
                                                self.response.write(u'<h4 style="padding-left:0.5cm;">Status id: {}</h4>'.format(str(tweet_id)))
                                                self.response.write(u'<h4 style="padding-left:0.5cm;">Reply id: {}</h4>'.format(str(reply_id)))
                                                
                                                score,tw = sentiment(text)
                                                if score < 0:
                                                    self.response.write(u'<h4 style="padding-left:0.5cm;">Negative, Score: {}, Tweet: {}</h4>'.format(score,tw))
                                                    neg_list.append(tw)
                                                elif score == 0:
                                                    self.response.write(u'<h4 style="padding-left:0.5cm;">Neutral, Score: {}, Tweet: {}</h4>'.format(score,tw))
                                                    nut_list.append(tw)
                                                else:
                                                    self.response.write(u'<h4 style="padding-left:0.5cm;">PositiveScore: {}, Tweet: {}</h4>'.format(score,tw))
                                                    pos_list.append(tw)
                                                
                                                scores.append(score)
                                                modified_tweet.append(tw)
                                        # print pos_list,neg_list,nut_list
                                        if pos_list or nut_list or neg_list:
                                            # self.response.write("""
                                            #     <canvas id="canvas_%s" height="450" width="600" style="padding-left:0.5cm;"></canvas>
                                            #     <p></p>
                                            #     <script>
                                            #     var pieData = [
                                            #             labels : ["January","February","March","April","May","June","July"],
                                            #             datasets : [
                                            #                     {
                                            #                         fillColor : "rgba(220,220,220,0.5)",
                                            #                         strokeColor : "rgba(220,220,220,1)",
                                            #                         data : %s
                                            #                     },
                                            #                     {
                                            #                         fillColor : "rgba(151,187,205,0.5)",
                                            #                         strokeColor : "rgba(151,187,205,1)",
                                            #                         data : %s
                                            #                     }
                                            #                        ]
                                            #                 ];
                                            #     var myPie = new Chart(document.getElementById("canvas_%s").getContext("2d")).Bar(pieData);
                                            #     </script>""" % (num,len(pos_list),len(nut_list),len(neg_list),num))
                                            self.response.write("""
                                                <canvas id="canvas_%s" height="250" width="250" style="padding-left:0.5cm;"></canvas>
                                                <p></p>
                                                <script>
                                                var pieData = [
                                                        {
                                                            value: %s,
                                                            color:"#F38630"
                                                        },
                                                        {
                                                            value : %s,
                                                            color : "#69D2E7"
                                                        },
                                                        {
                                                            value : %s,
                                                            color : "#E0E4CC"
                                                        }
                                                    ];
                                                var myPie = new Chart(document.getElementById("canvas_%s").getContext("2d")).Pie(pieData);
                                                </script>""" % (num,len(pos_list),len(nut_list),len(neg_list),num))
                                            num+=1
                                        else:
                                            self.response.write('No results yet!!!')
                                    
                            elif response.data.get('errors'):
                                self.response.write(u'Damn that error: {}!'.\
                                                    format(response.data.get('errors')))
                        else:
                            self.response.write('Damn that unknown error!<br />')
                            self.response.write(u'Status: {}'.format(response.status))



# Create a home request handler just that you don't have to enter the urls manually.
class Home(webapp2.RequestHandler):
    def get(self):
        
        # Create links to the Login handler.
        # self.redirect("/login/tw")
        self.response.write(""" <head>
                                <link href="/static/css/bootstrap.css" rel="stylesheet">
                                </head>
                                <body style="background-image:url(/static/img/tumblr.gif);background-size:100%;">
                                <h2>Allow <a href="/">Graphico</a> to access your Twitter Account.<br>Click on the links below.</h2>
                                <a href="/access/tw" class="btn btn-info btn-large"><i class="icon-white icon-envelope"></i> <h3>Post a Survey Question</h3></a>
                                <br>
                                <br>
                                <a href="/login/tw" class="btn btn-info btn-large"><i class="icon-white icon-envelope"></i> <h3>See the results</h3></a>""")


##########   main_2.py  ##############

"""

#########

main_2.py 

>>> posting survey question 

#########

"""

class Jumpin(webapp2.RequestHandler):
    def any(self, provider_name):
        
        # Log the user in.
        result = authomatic.login(Webapp2Adapter(self), provider_name)
        
        if result:
            if result.user:
                result.user.update()
                self.response.write('<h1>Hi {}</h1>'.format(result.user.name))
                
                # Save the user name and ID to cookies that we can use it in other handlers.
                self.response.set_cookie('user_id', result.user.id)
                self.response.set_cookie('user_name', urllib.quote(result.user.name))
                
                if result.user.credentials:
                    # Serialize credentials and store it as well.
                    serialized_credentials = result.user.credentials.serialize()
                    self.response.set_cookie('credentials', serialized_credentials)
                    
            elif result.error:
                self.response.set_cookie('error', urllib.quote(result.error.message))
            
            self.redirect('/action/tw')


class Home2(webapp2.RequestHandler):
    def get(self):
        # Create links to the Login handler.
        self.response.write('Login with <a href="/access/tw">Twitter</a>')
        
        # Retrieve values from cookies.
        serialized_credentials = self.request.cookies.get('credentials')
        user_id = self.request.cookies.get('user_id')
        user_name = urllib.unquote(self.request.cookies.get('user_name', ''))
        error = urllib.unquote(self.request.cookies.get('error', ''))
        
        if error:
            self.response.write('<p>Damn that error: {}</p>'.format(error))
        elif user_id:
            self.response.write('<h1>Hi {}</h1>'.format(user_name))
            
            if serialized_credentials:
                # Deserialize credentials.
                credentials = authomatic.credentials(serialized_credentials)
                
                # self.response.write("""
                # <p>
                #     You are logged in with <b>{}</b> and we have your credentials.
                # </p>
                # """.format(dict(fb='Facebook', tw='Twitter')[credentials.provider_name]))
                
                valid = 'still' if credentials.valid else 'not anymore'
                expire_soon = 'less' if credentials.expire_soon(60 * 60 * 24) else 'more'
                remaining = credentials.expire_in
                expire_on = credentials.expiration_date
                
                # self.response.write("""
                # <p>
                #     They are <b>{}</b> valid and
                #     will expire in <b>{}</b> than one day
                #     (in <b>{}</b> seconds to be precise).
                #     It will be on <b>{}</b>.
                # </p>
                # """.format(valid, expire_soon, remaining, expire_on))
                
                # <p>We can refresh them while they are valid.</p>
                #     <a href="refresh">OK, refresh them!</a>
                if credentials.valid:
                    self.response.write("""
                    
                    <p>Moreover, we can do powerful stuff with them.</p>
                    <a href="action/{}">Show me what you can do!</a>
                    """.format(credentials.provider_name))
                else:
                    self.response.write("""
                    <p>
                        Repeat the <b>login procedure</b>to get new credentials.
                    </p>
                    <a href="login2/{}">Refresh</a>
                    """.format(credentials.provider_name))
            
            self.response.write('<p>Goto results page</p>')
            self.response.write('<a href="/home">Results</a>')

            self.response.write('<p>We can also log you out.</p>')
            self.response.write('<a href="/logout2">OK, log me out!</a>')


class Refresh(webapp2.RequestHandler):
    def get(self):
        self.response.write('<a href="/home">Home</a>')
        
        serialized_credentials = self.request.cookies.get('credentials')
        credentials = authomatic.credentials(serialized_credentials)
        old_expiration = credentials.expiration_date
        
        response = credentials.refresh(force=True)
        
        if response:
            new_expiration = credentials.expiration_date
            
            if response.status == 200:
                self.response.write("""
                <p>
                    Credentials were refresshed successfully.
                    Their expiration date was extended from
                    <b>{}</b> to <b>{}</b>.
                </p>
                """.format(old_expiration, new_expiration))
            else:
                self.response.write("""
                <p>Refreshment failed!</p>
                <p>Status code: {}</p>
                <p>Error message:</p>
                <pre>{}</pre>
                """.format(response.status, response.content))
        else:
            self.response.write('<p>Your credentials don\'t support refreshment!</p>')
        
        self.response.write('<a href="">Try again!</a>')


class Action(webapp2.RequestHandler):
    def get(self, provider_name):
        if provider_name == 'tw':
            text = 'tweet'
        
        self.response.write("""<head><title>Researchr</title><link href="/static/css/bootstrap.css" rel="stylesheet"></head>
        <body style="background-color:#C6DAD5;">
        <br>
        <br>
        <a href="/" style="padding:0.5cm;">Home</a>
        <p style="padding-left:0.5cm;">Type your Survey Question here.</p>
        <form method="post" class="form-horizontal" role="form"  style="padding-left:0.5cm;">
            <input type="text" placeholder="Tweet Length(Max.100)" name="message" size="125" autofocus required/>
            <input type="submit" value="Do it!">
        </form>""".format(text))
    
    def post(self, provider_name):
        self.response.write('<a href="/home">Home</a>')
        
        # Retrieve the message from POST parameters and the values from cookies.
        message = '#tweet_survey '+self.request.POST.get('message')        
        serialized_credentials = self.request.cookies.get('credentials')
        user_id = self.request.cookies.get('user_id')
        
        if provider_name == 'tw':
            
            response = authomatic.access(serialized_credentials,
                                         url='https://api.twitter.com/1.1/statuses/update.json',
                                         params=dict(status=message),
                                         method='POST')
            
            error = response.data.get('errors')
            tweet_id = response.data.get('id')
            
            if error:
                self.response.write('<p>Damn that error: {}!</p>'.format(error))
            elif tweet_id:
                self.response.write("""
                <p>
                    You just tweeted a tweet with id {}.
                </p>
                """.format(tweet_id))
            else:
                self.response.write("""
                <p>
                    Damn that unknown error! Status code: {}
                </p>
                """.format(response.status))
        
        # Let the user repeat the action.
        self.response.write("""
        <form method="post">
            <input type="text" name="message" />
            <input type="submit" value="Do it again!">
        </form>
        """)


class Logout2(webapp2.RequestHandler):
    def get(self):
        # Delete cookies.
        # self.response.delete_cookie('user_id')
        # self.response.delete_cookie('user_name')
        # self.response.delete_cookie('credentials')
        
        # Redirect home.
        self.redirect('/')

"""

~~~~~~~~~~~~~~~~~~~~~~~~

Basic Sentiment Analysis

~~~~~~~~~~~~~~~~~~~~~~~~

"""

class Splitter(object):

    def __init__(self):
        self.nltk_splitter = nltk.data.load('tokenizers/punkt/english.pickle')
        self.nltk_tokenizer = nltk.tokenize.TreebankWordTokenizer()

    def split(self, text):
        """
        input format: a paragraph of text
        output format: a list of lists of words.
            e.g.: [['this', 'is', 'a', 'sentence'], ['this', 'is', 'another', 'one']]
        """
        sentences = self.nltk_splitter.tokenize(text)
        tokenized_sentences = [self.nltk_tokenizer.tokenize(sent) for sent in sentences]
        return tokenized_sentences

class POSTagger(object):

    def __init__(self):
        pass
        
    def pos_tag(self, sentences):
        """
        input format: list of lists of words
            e.g.: [['this', 'is', 'a', 'sentence'], ['this', 'is', 'another', 'one']]
        output format: list of lists of tagged tokens. Each tagged tokens has a form, a lemma, and a list of tags
            e.g: [[('this', 'this', ['DT']), ('is', 'be', ['VB']), ('a', 'a', ['DT']), ('sentence', 'sentence', ['NN'])],
                    [('this', 'this', ['DT']), ('is', 'be', ['VB']), ('another', 'another', ['DT']), ('one', 'one', ['CARD'])]]
        """

        pos = [nltk.pos_tag(sentence) for sentence in sentences]
        #adapt format
        pos = [[(word, word, [postag]) for (word, postag) in sentence] for sentence in pos]
        return pos

class DictionaryTagger(object):

    def __init__(self, dictionary_paths):
        files = [open(path, 'r') for path in dictionary_paths]
        dictionaries = [yaml.load(dict_file) for dict_file in files]
        map(lambda x: x.close(), files)
        self.dictionary = {}
        self.max_key_size = 0
        for curr_dict in dictionaries:
            for key in curr_dict:
                if key in self.dictionary:
                    self.dictionary[key].extend(curr_dict[key])
                else:
                    self.dictionary[key] = curr_dict[key]
                    self.max_key_size = max(self.max_key_size, len(key))

    def tag(self, postagged_sentences):
        return [self.tag_sentence(sentence) for sentence in postagged_sentences]

    def tag_sentence(self, sentence, tag_with_lemmas=False):
        """
        the result is only one tagging of all the possible ones.
        The resulting tagging is determined by these two priority rules:
            - longest matches have higher priority
            - search is made from left to right
        """
        tag_sentence = []
        N = len(sentence)
        if self.max_key_size == 0:
            self.max_key_size = N
        i = 0
        while (i < N):
            j = min(i + self.max_key_size, N) #avoid overflow
            tagged = False
            while (j > i):
                expression_form = ' '.join([word[0] for word in sentence[i:j]]).lower()
                expression_lemma = ' '.join([word[1] for word in sentence[i:j]]).lower()
                if tag_with_lemmas:
                    literal = expression_lemma
                else:
                    literal = expression_form
                if literal in self.dictionary:
                    #self.logger.debug("found: %s" % literal)
                    is_single_token = j - i == 1
                    original_position = i
                    i = j
                    taggings = [tag for tag in self.dictionary[literal]]
                    tagged_expression = (expression_form, expression_lemma, taggings)
                    if is_single_token: #if the tagged literal is a single token, conserve its previous taggings:
                        original_token_tagging = sentence[original_position][2]
                        tagged_expression[2].extend(original_token_tagging)
                    tag_sentence.append(tagged_expression)
                    tagged = True
                else:
                    j = j - 1
            if not tagged:
                tag_sentence.append(sentence[i])
                i += 1
        return tag_sentence

def value_of(sentiment):
    if sentiment == 'positive': return 1
    if sentiment == 'negative': return -1
    return 0

def sentence_score(sentence_tokens, previous_token, acum_score):
    if not sentence_tokens:
        return acum_score
    else:
        current_token = sentence_tokens[0]
        tags = current_token[2]
        token_score = sum([value_of(tag) for tag in tags])
        if previous_token is not None:
            previous_tags = previous_token[2]
            if 'inc' in previous_tags:
                token_score *= 2.0
            elif 'dec' in previous_tags:
                token_score /= 2.0
            elif 'inv' in previous_tags:
                token_score *= -1.0
        return sentence_score(sentence_tokens[1:], current_token, acum_score + token_score)

def sentiment_score(review):
    return sum([sentence_score(sentence, None, 0.0) for sentence in review])

def  sentiment(tweet):
    # tweet = """What can I say about this place. The staff of the restaurant is 
    # nice and the eggplant is not bad. http://Apart from that, very uninspired food, 
    # lack of atmosphere and too expensive. I am a @staunch vegetarian and was 
    # sorely dissapointed with the veggie options on the menu. Will be the last 
    # time I visit, I recommend others to avoid."""
    # tweet = """This is soRT dfffff loofffggggg  cool    http://ooo @cool"""
    tweet = tweet.lower()
    #Convert www.* or https?://* to URL
    tweet = re.sub('((www\.[\s]+)|(https?://[^\s]+))','URL',tweet)
    #Convert @username to AT_USER
    tweet = re.sub('@[^\s]+','AT_USER',tweet)
    #Remove additional white spaces
    tweet = re.sub('[\s]+', ' ', tweet)
    #Replace #word with word
    tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
    #trim
    tweet = tweet.strip('\'"')
    pattern = re.compile(r"(.)\1{1,}", re.DOTALL)
    tweet = pattern.sub(r"\1\1", tweet)
    # print tweet
    splitter = Splitter()
    postagger = POSTagger()
    dicttagger = DictionaryTagger([ 'dicts/positive.yml', 'dicts/negative.yml', 
                                    'dicts/inc.yml', 'dicts/dec.yml', 'dicts/inv.yml'])

    splitted_sentences = splitter.split(tweet)
    # pprint(splitted_sentences)

    pos_tagged_sentences = postagger.pos_tag(splitted_sentences)
    # pprint(pos_tagged_sentences)

    dict_tagged_sentences = dicttagger.tag(pos_tagged_sentences)
    # pprint(dict_tagged_sentences)
    score = sentiment_score(dict_tagged_sentences)
    return score,tweet



# SignUp & Login
"""
SignUp & Login
~~~~~~~~~~~~~~
"""
secret = 'imsosecret'

def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)

def make_secure_val(val):
    return '%s|%s' % (val, hmac.new(secret, val).hexdigest())

def check_secure_val(secure_val):
    val = secure_val.split('|')[0]
    if secure_val == make_secure_val(val):
        return val

def make_salt(length = 5):
    return ''.join(random.choice(letters) for x in xrange(length))

def make_pw_hash(name, pw, salt = None):
    if not salt:
        salt = make_salt()
    h = hashlib.sha256(name + pw + salt).hexdigest()
    return '%s,%s' % (salt, h)

def valid_pw(name, password, h):
    salt = h.split(',')[0]
    return h == make_pw_hash(name, password, salt)

def users_key(group = 'default'):
    return db.Key.from_path('users', group)

USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(username):
    return username and USER_RE.match(username)

PASS_RE = re.compile(r"^.{3,20}$")
def valid_password(password):
    return password and PASS_RE.match(password)

EMAIL_RE  = re.compile(r'^[\S]+@[\S]+\.[\S]+$')
def valid_email(email):
    return not email or EMAIL_RE.match(email)

def blog_key(name = 'default'):
    return db.Key.from_path('blogs', name)

def escape(txt):
    """Escape out special HTML characters in string"""
    return cgi.escape(txt, quote=True)    

def age_get(key):
    r = memcache.get(key)
    if r:
        val, save_time = r
        age = (datetime.utcnow() - save_time).total_seconds()
    else:
        val, age = None, 0
    return val, age

def age_set(key, val):
    save_time = datetime.utcnow()
    memcache.set(key,(val,save_time))


def add_post(ip, post):
    post.put()
    get_posts(update=True)
    return str(post.key().id())

def get_posts(update = False):
    q = Post.all().order('-created').fetch(limit = 10)
    mc_key = 'BLOGS'

    posts, age = age_get(mc_key)
    if update or posts is None:
        posts = list(q)
        age_set(mc_key, posts)
    return posts, age

class SiteHandler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        params['user'] = self.user
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

    def set_secure_cookie(self, name, val):
        cookie_val = make_secure_val(val)
        self.response.headers.add_header(
            'Set-Cookie',
            '%s=%s; Path=/' % (name, cookie_val))

    def read_secure_cookie(self, name):
        cookie_val = self.request.cookies.get(name)
        return cookie_val and check_secure_val(cookie_val)

    def login(self, user):
        self.set_secure_cookie('user_id', str(user.key().id()))

    def logout(self):
        self.response.headers.add_header('Set-Cookie', 'user_id=; Path=/')

    def initialize(self, *a, **kw):
        webapp2.RequestHandler.initialize(self, *a, **kw)
        uid = self.read_secure_cookie('user_id')
        self.user = uid and User.by_id(int(uid))
        self.format = 'html'

class User(db.Model):
    name = db.StringProperty(required = True)
    pw_hash = db.StringProperty(required = True)
    email = db.StringProperty()

    @classmethod
    def by_id(cls, uid):
        return User.get_by_id(uid, parent = users_key())

    @classmethod
    def by_name(cls, name):
        u = User.all().filter('name =', name).get()
        return u

    @classmethod
    def register(cls, name, pw, email = None):
        pw_hash = make_pw_hash(name, pw)
        return User(parent = users_key(),
                    name = name,
                    pw_hash = pw_hash,
                    email = email)

    @classmethod
    def login(cls, name, pw):
        u = cls.by_name(name)
        if u and valid_pw(name, pw, u.pw_hash):
            return u

class Post(db.Model):
    content = db.TextProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)

    def render(self):
        self._render_text = self.content.replace('\n', '<br>')
        return render_str("post.html", p = self)

    def as_dict(self):
        time_fmt = '%c'
        d = {'content': self.content,
             'created': self.created.strftime(time_fmt)}
        return d

# class PostPage(SiteHandler):
#     def get(self, post_id):
#         post_key = 'POST_' + post_id

#         post, age = age_get(post_key)
#         if not post:
#             key = db.Key.from_path('Post', int(post_id))#, parent=blog_key())
#             post = db.get(key)
#             age_set(post_key, post)
#             age = 0

#         if not post:
#             self.error(404)
#             return

#         self.render("permalink.html", post = post)
#         key = db.Key.from_path('Post', int(post_id), parent=blog_key())
#         post = db.get(key)
#         score,tweet = sentiment(post.content)
#         self.response.write("<br>analyzing text...<br>")
#         self.response.write(tweet+'<br>')
#         if score < 0:
#             self.response.write("Negative, Score: ")
#         elif score == 0:
#             self.response.write("Neutral, Score: ")
#         else:
#             self.response.write("Positive, Score: ")
#         self.response.write(score)

class NewPost(SiteHandler):
    def get(self):
        if self.user:
            self.render("newpost.html")
        else:
            self.redirect("/login")

    def post(self):
        if not self.user:
            self.redirect('/')

        content = self.request.get('content')

        if content:
            p = Post(parent = blog_key(), content = content)
            p.put()
            self.redirect('/%s' % str(p.key().id()))
        else:
            error = "content, please!"
            self.render("newpost.html", content=content, error=error)


class Signup(SiteHandler):
    def get(self):
        self.render("bootstrap-signup.html")

    def post(self):
        have_error = False
        self.username = self.request.get('username')
        self.password = self.request.get('password')
        self.verify = self.request.get('verify')
        self.email = self.request.get('email')

        params = dict(username = self.username,
                      email = self.email)

        if not valid_username(self.username):
            params['error_username'] = "That's not a valid username."
            have_error = True

        if not valid_password(self.password):
            params['error_password'] = "That wasn't a valid password."
            have_error = True
        elif self.password != self.verify:
            params['error_verify'] = "Your passwords didn't match."
            have_error = True

        if not valid_email(self.email):
            params['error_email'] = "That's not a valid email."
            have_error = True

        if have_error:
            self.render('bootstrap-signup.html', **params)
        else:
            self.done()

    def done(self, *a, **kw):
        raise NotImplementedError

class Register(Signup):
    def done(self):
        #make sure the user doesn't already exist
        u = User.by_name(self.username)
        if u:
            msg = 'That user already exists.'
            self.render('bootstrap-signup.html', error_username = msg)
        else:
            u = User.register(self.username, self.password, self.email)
            u.put()

            self.login(u)
            self.redirect('/welcome')

class Signin(SiteHandler):
    def get(self):
        self.render('login-form.html')
        # self.render('bootstrap-signin.html')


    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')

        u = User.login(username, password)
        if u:
            self.login(u)
            self.redirect('/welcome')
        else:
            msg = 'Invalid login! Please try again'
            self.render('login-form.html', error = msg)

class Logout(SiteHandler):
    def get(self):
        self.logout()
        self.redirect('/')

class Welcome(SiteHandler):
    def get(self):
        if self.user:
            self.render('index.html', username = self.user.name)
        else:
            self.redirect('/', username = "guest")

class Flush(SiteHandler):
    def get(self):
        memcache.flush_all()
        self.redirect('/')

class AboutPage(SiteHandler):
    def get(self):
        posts, age = get_posts()
        self.render('about.html',posts=posts)

class BlogFront(SiteHandler):
    def get(self):
        # posts, age = get_posts()
        # self.render('front.html', posts = posts)
        if self.user:
            self.render('index.html', username = self.user.name)
        else:
            self.render('index.html', username = "guest")
        


class TestClass(webapp2.RequestHandler):
    def get(self):
        self.response.write("""<script src="/static/chartjs/Chart.js"></script>
                                                <style>
                                                    canvas{
                                                    }
                                                </style>
                                                <canvas id="canvas" height="250" width="250"></canvas>
                                                <script>
                                                var pieData = [
                                                        {
                                                            value: %s,
                                                            color:"#F38630"
                                                        },
                                                        {
                                                            value : %s,
                                                            color : "#E0E4CC"
                                                        },
                                                        {
                                                            value : %s,
                                                            color : "#69D2E7"
                                                        }
                                                    ];
                                        var myPie = new Chart(document.getElementById("canvas").getContext("2d")).Pie(pieData);
                                        </script>""" % (30,40,90))

# app = webapp2.WSGIApplication([('/', BlogFront),
#                                ('/([0-9]+)', PostPage),
#                                ('/newpost', NewPost),
#                                ('/signup', Register),
#                                ('/login', Login),
#                                ('/home', Home),
#                                ('/access/tw', TweetAccess,'any'),
#                                ('/logout', Logout),
#                                ('/welcome', Welcome),
#                                ('/flush', Flush),
#                                ('/about', AboutPage),
#                                ],
#                               debug=True)
# Create routes.



ROUTES = [
            webapp2.Route(r'/login/<:.*>', Login, handler_method='any'),
            webapp2.Route(r'/test', TestClass),
            webapp2.Route(r'/home', Home),
            webapp2.Route(r'/', BlogFront),
            
            webapp2.Route(r'/newpost', NewPost),
            webapp2.Route(r'/signup', Register),
            webapp2.Route(r'/login', Signin),
            webapp2.Route(r'/logout', Logout),
            webapp2.Route(r'/welcome', Welcome),
            webapp2.Route(r'/flush', Flush),
            webapp2.Route(r'/about', AboutPage),
            
            webapp2.Route(r'/access/<:.*>', Jumpin, handler_method='any'),
            webapp2.Route(r'/refresh', Refresh),
            webapp2.Route(r'/action/<:.*>', Action),
            webapp2.Route(r'/logout2', Logout2),
            webapp2.Route(r'/home2', Home2)]


# Instantiate the webapp2 WSGI application.
app = webapp2.WSGIApplication(ROUTES, debug=True)
