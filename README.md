TweetSurvey
-----------

TweetSurvey is a Twitter API-based online survey tool.

    - Tweet your opinions
    - See the results
    - Magic


App URL:
-------

> [TweetSurvey] - Under Development


Developers:
----------

> Adithya  
> Jithin   
> Josina  
> Pallavi


Files:
-----
    /  
    ----
        |  
        |  
        |----/PyYaml-3.10/    
        |   
        |  
        |----/dicts/     
        |      
        |      
        |----/lib      
        |        
        |      
        |----/templates/      
        |
        |        
        |----/tokenizers/      
        |
        |        
        |----/taggers/
        |
        |        
        |----/nltk/              
        |      
        |       
        |----/README.md      
        |      
        |      
        |----/app.yaml      
        |      
        |      
        |----/sentiment.py     



Version:
-------

> 1.0


Requirements:
------------

> * [Python] - Python Version 2.7
> * [NLTK] - Natural Language Toolkit
> * [Twitter] - Twitter API
> * [Appengine] - Google Appengine for project hosting


Instructions:
------------
Make sure you have above requirements satisfied.

To run the app, install google appengine and go to appengine folder and type:

> $ ./dev_appserver.py ~/location-to-folder-containing-app/

Open web-browser and type in the address:

> http://localhost:8080


License
----
> Free for now

[Python]:http://www.python.org/download/releases/2.7.6/
[NLTK]:http://nltk.org/
[Appengine]:https://developers.google.com/appengine/docs/python/
[Twitter]:https://dev.twitter.com
[TweetSurvey]:http://www.tweetsurvey.appspot.com